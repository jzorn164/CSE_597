module GPUParallelMod
	implicit none
	
	contains
	
	subroutine HDF5_Write_Array(filename, len_string, rank, nprocs, start_column, end_column, Array)

		use HDF5
		use MPI
		
		implicit none
		
		!Input variables
		integer											:: rank, nprocs, start_column, end_column, len_string
		double precision, dimension(:,:), allocatable	:: Array
		character(len=len_string)						:: filename
		
		!Subroutine specific variables
		character(len=:), allocatable					::  dsetname
		integer(HID_T)									:: file_id, dset_id, file_space, mem_space, prop_list_id
		integer(HSIZE_T), dimension(2)					:: dims, count, offset
		integer											:: h_error, h_error_n, h_rank
		
		!MPI variables
		integer											:: ierr
		integer											:: comm, info
		
		!Subroutine source code
		comm = MPI_COMM_WORLD
		info = MPI_INFO_NULL
		
		h_rank = 2
		dsetname = trim(filename)
		!Open and Start HDF5
		call H5open_f(h_error)
		
		call h5pcreate_f(H5P_FILE_ACCESS_F, prop_list_id, h_error)
		call h5pset_fapl_mpio_f(prop_list_id, comm, info, h_error)
		
		!Create file collectively across processors
		call h5fcreate_f(trim(filename), H5F_ACC_TRUNC_F, file_id, h_error, access_prp = prop_list_id)
		call h5pclose_f(prop_list_id, h_error)
		
		dims(1) = size(Array,1); dims(2) = size(Array,2)
		!print*, dims
		count(1) = dims(1)
		count(2) = dims(2)/nprocs
		offset(1) = 0
		offset(2) = rank * count(2)
		
		!Create the data space
		call h5screate_simple_f(h_rank, dims, file_space, h_error)
		
		!Create the dataset
		call h5dcreate_f(file_id, dsetname, H5T_NATIVE_DOUBLE, file_space, &
						dset_id, h_error)
		call h5sclose_f(file_space, h_error)
		
		!Now we need to describe the dataset necessary on each processor
		! dims = size(Array)
		! count(1) = dims(1)
		! count(2) = dims(2)/nprocs
		! offset(1) = 0
		! offset(2) = h_rank * count(2)
		call h5screate_simple_f(h_rank, count, mem_space, h_error)
		
		call h5dget_space_f(dset_id, file_space, h_error)
		call h5sselect_hyperslab_f(file_space, H5S_SELECT_SET_F, offset, count, h_error)
		
		call h5pcreate_f(H5P_DATASET_XFER_F, prop_list_id, h_error)
		call h5pset_dxpl_mpio_f(prop_list_id, H5FD_MPIO_COLLECTIVE_F, h_error)
		
		call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, Array(:,start_column:end_column),dims, h_error, &
						file_space_id = file_space, mem_space_id = mem_space, xfer_prp = prop_list_id)
		
		call h5sclose_f(file_space, h_error)
		call h5sclose_f(mem_space, h_error)
		
		call h5dclose_f(dset_id, h_error)
		call h5pclose_f(prop_list_id, h_error)
		
		call h5fclose_f(file_id, h_error)
		
		call h5close_f(h_error)
		
	end subroutine HDF5_Write_Array
	
	subroutine DiffFind_FDM(xMatrix,xMatrixOld,bMatrix,column_start,column_end,columns_glo,rank,nprocs,dx,dy,rows,nx,ny,locRes)
		
		use openacc
		
		implicit none

		integer											:: i, j, nx, ny
		integer											:: column_end, column_start, columns_glo, rows, rank, nprocs
		double precision								:: locRes, dx, dy
		double precision, dimension(nx,ny)				:: bMatrix, xMatrix, xMatrixOld
	
		if (rank.eq.0 .and. nprocs.eq.1) then
		!$acc kernels
			do j = 2,columns_glo-1
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
					locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
					xMatrixOld(i,j) = xMatrix(i,j)
				enddo
			enddo
		!$acc end kernels
		elseif (rank.eq.0 .and. nprocs.ne.1) then
		!$acc kernels
			do j = column_start+1,column_end
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
					locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
					xMatrixOld(i,j) = xMatrix(i,j)
				enddo
			enddo
		!$acc end kernels
		elseif (rank.eq.nprocs-1) then
		!$acc kernels
			do j = column_start,column_end-1
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
					locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
					xMatrixOld(i,j) = xMatrix(i,j)
				enddo
			enddo
		!$acc end kernels
		else
		!$acc kernels
			do j = column_start, column_end
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
					locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
					xMatrixOld(i,j) = xMatrix(i,j)
				enddo
			enddo
		!$acc end kernels
		endif
		
	end subroutine DiffFind_FDM
	
	subroutine DiffFind(xMatrix,column_end,column_start,xMatrixOld,locRes,rows,rank, nx, ny)
		
		use openacc
		
		implicit none

		integer											:: i,j, nx, ny
		integer											:: column_end, column_start, rows, rank
		double precision								:: locRes
		double precision, dimension(nx,ny)				:: xMatrix, xMatrixOld
		
		locRes = 0.0
		!$acc parallel loop gang reduction(max:locRes)
		do j = column_start,column_end
			do i = 1,rows
				locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
				xMatrixOld(i,j) = xMatrix(i,j)
			enddo
		enddo
		!$acc end parallel
		
	end subroutine DiffFind
	
	subroutine FDM(xMatrix,xMatrixOld,bMatrix,column_start,column_end,columns_glo,rank,nprocs,dx,dy, rows,nx,ny)
	
		use openacc
	
		implicit none
		
		integer											:: nx,ny
		integer											:: i,j
		integer, intent(in)								:: column_end, column_start, columns_glo, rows, rank, nprocs
		double precision, dimension(nx,ny)				:: bMatrix,xMatrix,xMatrixOld
		double precision, intent(in)					:: dx,dy
		
		if (rank.eq.0 .and. nprocs.eq.1) then
		!$acc parallel loop gang
			do j = 2,columns_glo-1
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
				enddo
			enddo
		!$acc end parallel
		elseif (rank.eq.0 .and. nprocs.ne.1) then
		!$acc parallel loop gang
			do j = column_start+1,column_end
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
				enddo
			enddo
		!$acc end parallel
		elseif (rank.eq.nprocs-1) then
		!$acc parallel loop gang
			do j = column_start,column_end-1
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
				enddo
			enddo
		!$acc end parallel
		else
		!$acc parallel loop gang
			do j = column_start, column_end
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
				enddo
			enddo
		!$acc end parallel
		endif
	
	end subroutine FDM
	
	subroutine Setup_Broadcast(nx, ny, CBC, CSOL, dx, dy, IFlag, eps, maxIter)
	
		implicit none
		include 'mpif.h'
		integer											:: nx, ny, CBC, CSOL, IFlag, ierr, maxIter
		double precision								:: dx, dy, eps
		
		call MPI_Bcast(nx,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(ny,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(CBC,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(CSOL,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(IFlag,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(dx,1,MPI_Double_Precision,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(dy,1,MPI_Double_Precision,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(eps,1,MPI_Double_Precision,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(maxIter,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		
		call MPI_Barrier(MPI_COMM_WORLD,ierr)
		
	end subroutine Setup_Broadcast
	
	subroutine Setup_Matrix(xMatrix,xMatrixOld,bMatrix,columns_glo,rows)
	
		implicit none
		
		integer											:: rows, columns_glo
		double precision, dimension(:,:), allocatable	:: bMatrix, xMatrix, xMatrixOld
		
		allocate(xMatrix(rows,columns_glo),bMatrix(rows,columns_glo),xMatrixOld(rows,columns_glo))
		
		xMatrix = 0.0
		xMatrixOld = 0.0
		bMatrix = 0.0
		bMatrix(rows/2,columns_glo/2) = 1.0
	
	end subroutine Setup_Matrix
	
	subroutine Setup_General(columns_glo,rows,columns_loc,column_start,column_end,nx,ny,nprocs,rank)
	
		implicit none
		include 'mpif.h'
		integer											:: nx, ny, rows, columns_glo, columns_loc
		integer											:: rank, nprocs, column_end, column_start
		
		columns_glo = ny
		rows = nx
		columns_loc = (columns_glo)/nprocs
		
		if (rank.eq.0) then
			column_start = (rank) * columns_loc + 1
			column_end = (rank+1) * columns_loc
		else
			column_start = (rank) * columns_loc + 1
			column_end = (rank+1) * columns_loc
		endif
		
		if (rank.eq.nprocs-1 .and. nprocs.gt.1) then 
			column_end = columns_glo
		endif
		
	end subroutine Setup_General
	
	subroutine Read_Input_File(nx, ny, CBC, CSOL, dx, dy, IFlag, maxIter)
		implicit none
		
		integer, intent(out)							:: nx, ny, cbc, csol, IFlag, maxIter
		real*8, intent(out)								:: dx, dy
		integer											:: ierror, i_value
		integer											:: comment_pos, equal_pos
		real											:: r_value
		character*80									:: line, inputfile, tag, comment
		
		inputfile = 'input.in'
		Open(unit=14,file=trim(inputfile),status='old',action='read')
		
		do
			read(14, '(a)',iostat=ierror) line
				if (ierror.ne.0) then
					exit
				endif
			comment_pos = index(line,'#')
			
			if (comment_pos.eq.1) then
			else
				read(line,*,iostat=ierror) tag
				select case (tag)
					case ('NX')
						read(line,*,iostat=ierror) tag, r_value, comment
						nx = int(r_value)
					case ('NY')
						read(line,*,iostat=ierror) tag, r_value, comment
						ny = int(r_value)
					case ('CBC')
						read(line,*,iostat=ierror) tag, r_value, comment
						cbc = int(r_value)
					case ('CSOL')
						read(line,*,iostat=ierror) tag, r_value, comment
						csol = int(r_value)
					case ('DX')
						read(line,*,iostat=ierror) tag, r_value, comment
						dx = r_value
					case ('DY')
						read(line,*,iostat=ierror) tag, r_value, comment
						dy = r_value
					case ('IFLAG')
						read(line,*,iostat=ierror) tag, r_value, comment
						IFlag = int(r_value)
					case ('MAXITER')
						read(line,*,iostat=ierror) tag, r_value, comment
						maxIter = int(r_value)
				end select
			endif
		enddo
		close(14)
		
	end subroutine Read_Input_File
end module GPUParallelMod