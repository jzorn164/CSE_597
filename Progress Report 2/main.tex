\documentclass{article}
% Change "article" to "report" to get rid of page number on title page
\usepackage{amsmath,amsfonts,amsthm,amssymb}
\usepackage{setspace}
\usepackage{Tabbing}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{extramarks}
\usepackage{url}
\usepackage{chngpage}
\usepackage{longtable}
\usepackage{soul,color}
\usepackage{graphicx,float,wrapfig}
\usepackage{enumitem}
\usepackage{morefloats}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{indentfirst}
\usepackage{lscape}
\usepackage{pdflscape}
\usepackage{natbib}
\usepackage[toc,page]{appendix}
\providecommand{\e}[1]{\ensuremath{\times 10^{#1} \times}}

% In case you need to adjust margins:
\topmargin=-0.45in      % used for overleaf
%\topmargin=0.25in      % used for mac
\evensidemargin=0in     %
\oddsidemargin=0in      %
\textwidth=6.5in        %
%\textheight=9.75in       % used for mac
\textheight=9.25in       % used for overleaf
\headsep=0.25in         %

% Homework Specific Information
\newcommand{\hmwkTitle}{Progress Report 2}
\newcommand{\hmwkDueDate}{Monday,\ November\  5,\ 2018}
\newcommand{\hmwkClass}{Final Project}
\newcommand{\hmwkClassTime}{CSE 597}
\newcommand{\hmwkAuthorName}{Jacob\ A.\ Zorn}
\newcommand{\hmwkNames}{jzz164}

% Setup the header and footer
\pagestyle{fancy}
\lhead{\hmwkNames}
\rhead{\hmwkClassTime: \hmwkTitle} 
\cfoot{Page\ \thepage\ of\ \pageref{LastPage}}
\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make title

\title{\vspace{2in}\textmd{\textbf{\hmwkClass:\ \hmwkTitle}} \\
\vspace{0.1in}\large{ \hmwkClassTime}\vspace{3in}}

\author{\textbf{\hmwkAuthorName} \\ \vspace{0.1in}
\hmwkDueDate }
\date{} % to take away today's date

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\begin{spacing}{1.1}
\maketitle

\newpage
\section*{Abstract}

In this report, I report on an program to approximate a solution to Poisson's equation. Poisson's equation is used in a wide variety of the fields ranging from electrostatics to computational fluid dynamics. Utilizing an iterative solving method, the Jacobi method, an approximate solution is found and the code is further parallelized using the Message Passing Interface (MPI). Strong scaling results indicates great scaling ability and maintains high computational efficiency at 8 processors. It is expected that if more processors are utilized in this scaling project we could expect similiar results at higher number of processors.

\section{Problem of Interest}

The problem we are discussing here is Poisson's equation for describing the electric potential, and therefore electric field, in a material. Poisson's equation can be seen below in (1), \\
      \begin{equation} \label{eq1}
      \Delta U = f(x,y).
      \end{equation} \\
      \indent Understanding the distribution of the electric field is necessary for proper simulation of ferroelectric thin film systems in order to correctly approximate the free energy contribution of an electric field. This contribution leads to the development of the common head-to-tail domain structures that exist in materials such as lead titanate (PTO), lead titanate zirconate (PZT), and barium titanate (BTO). Therefore, allowing for correct simulation and prediction of the domain structures that may appear in not only those systems but also other ferroelectric material systems. Not only is it imperative to correctly approximate the electric field in ferroelectric and other electronic materials, but also in the modeling and simulation of magnetic materials.\\
      
      A ferroelectric domain structure or state is traditionally defined as a spatial distribution of regions of aligned electric dipoles, separated by domains across which the polarization of the dipoles changes\citet{moulson}. These domain structures or states change in orientation and polarization as to minimize the total free energy of the system. The free energy of these systems are dependent on a number of factors such as, including but not limited to, chemical free energy (think chemical bonding or elemental composition), elastic or mechanical free energy, and electrostatic energy. The sum of these free energy contributions create what we term a \textit{Free Energy Functional} and can be described by (2) below,\\
        \begin{equation} \label{eq2}
        	\int_V f_{chemical}(P_{i}) + f_{elastic}(P_{i},\eta_{ij}) + f_{electric}(P_{i},E_{i})  dV = F(P_{i},\eta_{ij},E_{i}).
        \end{equation}\\
        \indent This free energy is necessary for the various evolution equations that are utilized to model microstructural evolution of materials. The Allen-Cahn Equation (3) and Cahn-Hillard Equation (4) are the two equations used to simulate this processes. Here \textit{Q} and \textit{R} are order parameters that characterize some phase of the material. More information regarding solving methods and applications of these equations can be found in \cite{biner2017,chenreview}.
        \begin{equation} \label{eq3}
        	\frac{\partial Q}{\partial t} = -L \frac{\delta F}{\delta Q}
        \end{equation}
        \begin{equation} \label{eq4}
        	\frac{\partial Q}{\partial t} = \nabla M \nabla \frac{\delta F}{\delta R}
        \end{equation}\\
      	\indent Therefore, we can see it is necessary to fully understand the electrostatic potential and the electric field that can develop due to these dipoles in a material system. Thus an accurate solution to Poisson's equation is necessary to fully understand this microstructural evolution.\\
      	
      	\indent Poisson's Equation is necessary to solve because so many different physical processes can be described by Poisson's equation or the analogous Laplace equation. With applications ranging from magneto-static and electrostatic equilibrium to heat transfer and beyond, it is widely known that Poisson's equation has a range of relevant fields. Additionally, this is also necessary for understanding the electric contribution to the free energy of a material system so the domain structure can be modeled properly.\\

        \indent Poisson's equation shows up in a variety of scientific and engineering fields. Not only do material engineers and scientists use Poisson's equation to derive electric field but computational fluid dynamics modelers also use Poisson's equation \cite{Hirt}. The homogeneous form of the Poisson's equation, where the right hand side is equal to zero, is commonly referred to as the Laplace equation. The Laplace equation is used predominately in the field of thermal modeling and steady state heat transfer.\\
        
        \indent There are a variety of ways of solving Poisson's equation, and they are all dependent on the boundary conditions imposed on the system in question. For example, as will be discussed a parallel plate model of Poisson's equation allows the use of Successive Over-Relaxation (SOR) whereas a singular charge or dipole charge does not allow for the use of Successive Over-Relaxation. Therefore, in cases where SOR can't be utilized a better more stable method must be used, such as Jacobi relaxation. Analytically solutions also exist and are commonly found in Fourier space through the use of Fourier Transforms. A common solution utilized in the phase field modeling of ferroelectric domain structures is described by Li \textit{et al.} \cite{Li2002,Li2004}.\\
        
        \indent The solution will be replicating in this paper comes from the work of Ian Cooper at the University of Sydney. We will replicating the effect of a single charge at the origin of a two dimensional plate, thus solving Poisson's equation in two dimensions \cite{Cooper}. The code will be produced in such a way that the user can easily implement different boundary conditions or initial conditions depending on the particular setup at hand. We wish to understand the electric potential and, by appropriate derivation, the electric field. We can then utilize the electric field we derive to determine the electrostatic contribution to the free energy of a Ferroelectric system. Note that we do not calculate this contribution and this program would only be used as an additional module or library to model the microstructural evolution. We can see from the work of Cooper, and of this work, that while the point charge at the origin is infinitely steep, the potential that is derived from that has a defined slope which gives rise to an electric field, which permeates through the entire plate.

\section{Parallelization}
    
    \indent For this project I chose to utilize Message Passing Interface (MPI) for the parallelization of the Poisson's Equation Solver, over some of the other available methods such as OpenMP or PThreads. Part of this reason, is the stronger scaling ability of MPI versus a threaded infrastructure. In addition, running in this manner relieves the user of having to export the OpenMP environment variable during the compiling and batch job scripts. Whereas in MPI there is no need to set the number of processors for a given code, unless the user wants to evaluate the scaling ability of the code from an MPI standpoint. \\
    
    \indent For this code, I paralellized the work being done by each process. For example, I was able to decompose the large domain, termed xMatrix in the source code, in a 1D type of decomposition. This provides two advantages: firstly, since the code was written in Fortran and Fortran is row major, decomposing along a column allows for easy accessing of the data; secondly, the 1D decomposition also reduces the amount of communication I have to do with neighboring cells. While regular intuition would tell us that perhaps breaking a block into 4 smaller blocks is an intelligent way of decomposing matrix, it wouldn't necessaryily work if we only had 3 processors to run parallel on. This style of domain decomposition allows easy scaling no matter the number of processors thrown at the code.\\
    
    \indent Once, the domain was split up among the different processors, the number of array elements that each processor is responsible for decreased. The reduction in responsible elements allowed for faster calculation of Poission's equation. Judging from the serial code profiles, which are presented and discussed in detail below, I think approximately 50-70\% of the code is parallelized, since so much of the time consuming routines are dependent on the number of elements each processor is responsible for.\\
    
    \indent There is no difference in the way the A-matrix, X-vector, or b-vectoris setup in this code, because the code initalizes the same matrices on each processor. However, each processor is only responsible for certain number of those elements. Now it should be noted that if/when a production size model is run using this code, it could prove advantageous from a memory prospective to only provide each processor with the appropriate elements it needs to calculate.

\section{Profiling}

\subsection{Serial}

The Poisson Solver code was profiled using the Tau profiling software available from the University of Oregon \cite{taurefer}. The profile is shown below in \textbf{Figure 1}. Breaking down the profile we can see that the two subroutines take up the majority, approximately 90\%, of the runtime of the code. Thus during the parallelization of the code will be necessary to find a way to speed up that portion of the code. I would expect the code to produce a profile similar to what to what the profile shows. Additionally, since the main program is writing the answer to a data file, I don't foresee a way to speed up that portion of the code from a parallel perspective because that could cause file lock error.\\

        \begin{figure}
        \begin{center}
        \includegraphics[width=0.6\linewidth]{SerialProfile1.png}
        \caption{Profile of the serial Poisson's Equation Solver, the blue bar refers to the Finite Difference Method (FDM) routine, the red bar refers to the Error Finder routine (DiffFind), and the green bar refers to the rest of the main program.}
        \label{fig:Fig1}
        \end{center}
        \end{figure}

\indent There are few potential ideas I have to improve upon for the serial code and these are discussed in the following list. For the initial serial code it should be noted that FDM and DiffFind operate as two independent sets of loops, this type of setup provides for the ability to perhaps find a way to speed up the code.
\begin{itemize}
    \item Move the DiffFind routine into the FDM routine. This will eliminate the extra loops and therefore reduce the number of times each array element is being called.
    \item Run DiffFind only every 5 or 10 iterations. Since I am aware that it will take 1000+ iterations to solve the entire system, only checking the error every 5 or 10 iterations will eliminate perhaps hundreds of DiffFind routine calls.
    \item Stagger the FDM routine, so instead of each call accessing each array element, if I stagger the routine then I will only be accessing half of the array elements on any given call. However, this could perhaps increase the number of iterations for the total solving of the system.
\end{itemize}

\indent For the first step I took to speed up the serial code I changed how often the DiffFind routine is called to once every 5 iterations, and then again once every 10 iterations. Presented in \textbf{Figure 2} is the profile of the modified code. We can see that while the FDM routine still possesses a great deal of the time. However, we can see the number of DiffFind calls is greatly reduced, which leads to a faster overall code. The time to run decreased by approximately 40\% compared to the originally unmodified code. \\

\indent Continuing we changed the number of iterations it checked for converenge to once every tenth iteration. Therefore, we greatly reduced the number of DiffFind calls once more. The results of this modification can be seen in \textbf{Figure 3}. Once again we see a decrease in the timing of the code and a decrease in how often the DiffFind routine is called, as expected. However, there is little to no time saved in terms of overall runtime by decreasing the frequency of error checking.\\

The last modification I made to my code, was to combine the FDM and DiffFind routines to reduce the extra loops that run as a result of the DiffFind function. That profile is seen in \textbf{Figure 4}. Now while most of the profile is taken up by a new routine, termed "DiffFind-FDM", the total run time of the code is now longer because the code breaks down. This code break down is due a failure for the code to monitor the maximum error.

    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{SerialProfile2.png}
        \caption{Profile of the modified Serial Poisson's Equation Solver, the colors of the bars coordinate with the previously posted picture, we can see that the DiffFind routine is greatly reduced}
        \label{fig:Fig2}
    \end{figure}
    
    \begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{SerialProfile3.png}
    \caption{The frequency of the DiffFind routine decreased once again, which helped to reduce the runtime, but only slightly. Therefore, we can see that finding a way to reduce the frequency of the FDM routine is perhaps the best way to speed up this code.}
    \label{fig:Fig3}
    \end{figure}
    
    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{SerialProfile4.png}
        \caption{It is apparent the moving the DiffFind routine into the FDM routine doesn't result in any improvement in either the number of iterations or the runtime of the code. This is due to communication portion of the code and the overall construction of my code. Therefore, this kind of improvement will not increase the ability of the code.}
        \label{fig:Fig4}
    \end{figure}

\subsection{Parallel}

\indent The same Tau profiler that was used for the serial code was utilized to profile the parallel code. An inital profile can be seen in \textbf{Figure 5}. It can be readily seen that a great deal of the time is spent where the all of the processors aside from the root process (node 0) are idle in an MPI Barrier routine. This chunk of time is also related to the additional main program time that the root processor handles. This is due to the output method of the code, and this may be position of future improvement of the code. \\

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{ParallelProfile2.PNG}
    \caption{Looking at the profile, it is apparant that the longest, most time consuming point of the code is a collection of MPI Barrier routines. While currently there is no way to reduce the MPI Barrier routines because of the input and output of the data files. However, it is also necessary to utilize reduce how often the DiffFind routines are utilized.}
    \label{fig:Fig5}
\end{figure}

\indent I would expect the profile that I see for the parallel code, due to the amount of communication in the code. Additionally, from the profile we can see that there is a great deal of time taken up by the DiffFind and FDM routines as was seen in the serial code. [Need to talk about memory here]\\

\indent In addition to the improvement mentioned earlier in regard to outputting the data file, there are a few improvements that this code could benefit from and they are outlined below.\\

\begin{itemize}
    \item As I did for the serial code, we can stagger the frequency we check for error convergence. Therefore reducing how often we must do the extra array looping with the DiffFind routine.
    \item In a similar way to the serial code, I can incorporate the DiffFind routine into the FDM routine to eliminate the need for the additional loops, while not sacrificing the need to run additional iterations after the convergence has reached the appropriate value.
    \item Finding a method to increase the efficiency of the output of the file. It could be advantageous to have each individual processor output their array to an individual file.
\end{itemize}

\indent Utilizing the "/usr/bin/time -v" command on the Linux cluster, I was able to monitor the amount of memory usage of the code. When we run the code in serial mode we see the average memory is in line with what we would expect when calculating the amount of necessary memory. However, as we begin to run in parallel using the "mpirun" command, we see an increase in memory usage, probably attributed to the additional overhead and communication between the different processors.\\

\indent For the first improvement of the code, I reduced the frequency of the DiffFind routine being run. First, I changed the frequency from every iteration to every 5th iteration and this profile can be seen below in \textbf{Figure 6}. It is apparent that there was a code speedup and a great reduction in the number of times we called the routine. I then reduced the frequency once more to every 10th iteration and this profile is demonstrated in \textbf{Figure 7}. While we see a reduction in DiffFind calls, we don't see a decrease in the total runtime of the code.\\

Therefore, I decided to reduce the frequency of MPI communication routines. The profile shown in \textbf{Figure 8} demonstrates the updated profile of reduced MPI communication routines. The reduction in communication helps in reducing then number of iterations necessary for finding the solution of Poisson's Equation. Therefore, I think pairing this method with other modifications could result in a larger decrease in both runtime of the code and iterations necessary to find a solution. \\

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{ParallelProfile3.PNG}
    \caption{Changing the frequency of the DiffFind function, greatly reduces how often the routine is called and thus saves in runtime.}
    \label{fig:fig6}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{ParallelProfile4.PNG}
    \caption{Once again the frequency was reduced for calling the DiffFind function, but this didn't result in a decrease in total code runtime as was noted in the initial frequency change.}
    \label{fig:fig7}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{ParallelProfile6.PNG}
    \caption{Reducing the communication between nodes leads to overall less iterations necessary to find the approximate solution.}
    \label{fig:fig8}
\end{figure}

\section{Scaling}

\subsection{Strong Scaling}

\indent To understand the scaling speedup and scaling efficiency of the code, strong scaling analysis was applied to this work. Below are a a few of the key plots for evaluating parallel programming paradigms. In \textbf{Figure 9}, the strong scaling speedup of the work is shown. It can be seen from the figure that when the system size is 100x100 grid points, the parallel speedup begins to level off at around 5 processors. I attribute this to the smaller system size of those runs. However, as the system size is increased the scaling speedup more closely follows what we might expect from parallel programming.\\

\indent It is also necessary to understand the efficiency of parallel programming to make the best use of the system. The system efficiency is plotted in \textbf{Figure 10}. In the same way when discussing the speed up of small systems, the efficiency also suffers and thus if one was running small systems than running a small number of processors would be preferred. However, it is apparent that up to 8 processors the code is relatively efficient and maintains a high efficiency ($>$80\%), which can be helpful when running production scale problems.\\

\indent If I was looking for a faster answer, I would run at a large amount of processors, which in this case would be 8 processors, since that is max I tasted. As for maximizing the efficency of the system, I would still probably choose 8 processors because the efficiency is still high and I would only lose approximately 15\% efficiency between 2 processors and 8 processors but would solve the equation more than 3 times as quickly. This line of thinking would also apply to research size problems.\\

\indent Lastly, I plotted the results of my scaling study to Amdahl's law. In order to understand what percent of my code was parallelized, I plotted Amdahl's law for various percentages. From here I was able to approximate that my code was 96\% parallelized, while still maintaining approximately 4\% of serial code. Additionally, as we see from \textbf{Figure 11}, the homegrown code follows closely to 97\% parallelized Amdahl's law, which agrees with the previous analysis.

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{Scaling1.png}
    \caption{As evidenced by the plot, it can be seen that the parallel programming scales quite well with the increase in the number of processors.}
    \label{fig:fig9}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width = 0.6\linewidth]{Scaling2.png}
    \caption{It is evident that the code is relatively efficient as the system size is scaled up. While the efficiency of the smaller systems suffer, the larger systems that more closely approximate what may be encountered in research gives good hope moving forward.}
    \label{fig:fig10}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{Amdahl.png}
    \caption{The plot shows various levels of paralellized code using Amdahl's law as a reference point. As we see here, the homegrown code matches up quite well with Amdahl's law in regards to scaling.}
    \label{fig:fig11}
\end{figure}

\section{Discussion and Conclusions}

\indent In this project, I was able to create a Poisson's equation solver for deriving the electric field distribution for a domain structure inside a ferroelectric material. This is helpful for understanding the electrostatic energy contribution to the total free energy of the system. The code was parallelized and compared to its serial version to test for the scalability and efficiency of the parallelization. It was found that the code scaled quite well and maintained high efficency ($>$80\%) at 8 processors while running almost 6.75 times faster than the serial version.\\

\indent The Message Passing Interface (MPI) was utilized for making this code parallel. This was done due to the enhanced scalability of MPI and ability to scale up to thousands of processors if necessary. From this work it became apparent that MPI was perhaps the best choice for parallelizing this code. Also, with MPI there requires no additional environment variables to be described or defined like might be necessary for another parallelization technique such as OpenMP.\\

\indent The code was written with Dirichlet Boundary conditions and the domain was decomposed in 1D along the so-called "y" direction fo the system. This was to assist in reducing the overall communication of the code. For the future of the project, I intend to introduce a SOR scheme to aid in finding an approximate solution faster. This is due to the fact that since at each time step of a domain structure simulation Poisson's equation but must solved, then the faster the equation is solved the faster the structure simulation can occur. Therefore, it is obvious that this section of the code must be fast to accomplish the structure simulation.

\newpage
\begin{appendices}

\section{Acknowledgements}

I would like to thank Dr. Lavely and Dr. Blanton for their teaching and expertise to help me parallel this code and being available to answer questions regarding both parallel and general programming.

\section{Code}

\begin{itemize}
    \item Where can you find the code
        \begin{itemize}
            \item The code can be found on my gitlab at "https://gitlab.com/jzorn164/CSE\_597/"
        \end{itemize}
    \item File names and descriptions
        \begin{itemize}
            \item Filename: Parallel.f90
                \begin{itemize}
                    \item This is the main program for running the code, which only contains the main program code. For the different subroutines that are called, please see the following file.
                \end{itemize}
            \item Filename: ParallelMod.f90
                \begin{itemize}
                    \item This is the module file which contains all of the different subroutines for running the code in this project. There is no special way to compile it and thus can be compiled using the provided Makefile.
                \end{itemize}
            \item Filename: input.in
                \begin{itemize}
                    \item This is the input file which contains the size of the system being modeled/simulated. For this project, we always utilized the a maximum number of iterations set at 10,000 and varied the system size from 100x100 to 500x500. Which varies the number of grid points from 10,000 to 250,000 grid points, which provides a great starting point for analysis.
                \end{itemize}
            \item Filename: Timer.sh
                \begin{itemize}
                    \item This is bash script that allows for testing the various system sizes and can be submitted as a batch job to perform scaling studies. 
                \end{itemize}
        \end{itemize}
    \item Instructions for compiling and reproducing your results
        \begin{itemize}
            \item To compile and reproduce the results presented in this report, simply run the Makefile by using the "make" command. Then the scaling study can be reproducing by submitting the PBS batch file located in the Gitlab repository. This batch file runs the code with a variety of different number of processors and system sizes.
        \end{itemize}
    \item Listing of which nodes types you ran on 
        \begin{itemize}
            \item All of the codes and scaling studies were ran on basic ACI-B nodes.
        \end{itemize}
\end{itemize}

\section{Poster Draft - separate document}
\indent A screenshot of the poster can be seen below. It is also located on the Gitlab repository.

\begin{figure}
    \centering
    \includegraphics[width = 0.6\linewidth]{Poster.png}
    \caption{Screenshot of the Poster Draft}
    \label{fig:fig12}
\end{figure}


\end{appendices}


\bibliographystyle{acm}
\bibliography{progressReport}

\end{spacing}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%}}

